#!/usr/bin/python
from math import radians, sin, cos
from time import sleep
import pygame
from pygame.locals import *
import sys
# B/this is to include the assets folder to import from
import random
from ship import ship as Ship
from landingpad import landingpad as Landingpad
from terrain import terrain as Terrain

global score
score = 0

def main():
	black = 0,0,0
	white = 255,255,255
	red = 255,0,0
	green = 0,255,0

	pygame.font.init()
	font = pygame.font.SysFont('Comic Sans MS', 30)

	size = width, height = 1200,720
	screen = pygame.display.set_mode(size)
	pygame.display.set_caption('Asteroids')
	clock = pygame.time.Clock()
	fps = 60
	turnspeed = 1.0/float(fps)
	
	pygame.joystick.init()
	if pygame.joystick.get_count() > 0:
		joystick = pygame.joystick.Joystick(0)
		joystick.init()
	
	global winstate, ship, landingpad, terrain, score
	
	winstate = 'Lost'

	ship = Ship(screen, turnspeed)
	landingpad = Landingpad(screen)
	terrains = []
	for i in xrange(9):
		terrains.append(Terrain(screen))

	done = False
	while not done:
		global flame
		flame = False
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				done = True
		keys = pygame.key.get_pressed()
		#if keys[K_r] or joystick.get_button(11):
		if keys[K_r]:
			main() 
		#if keys[K_q] or joystick.get_button(10):
		if keys[K_q]:
			pygame.quit()
		#if keys[K_UP] or joystick.get_button(1) or joystick.get_axis(1) == -1.000 or joystick.get_hat(0) == (0,1):
		if keys[K_UP]:
			if ship.fuel != 0:
				ship.velocity['x'] += turnspeed*ship.thrust*sin(radians(ship.heading))
				ship.velocity['y'] -= turnspeed*ship.thrust*cos(radians(ship.heading))
				flame = True
				ship.fuel -= 1
		#if keys[K_LEFT] or joystick.get_hat(0) == (-1,0) or joystick.get_axis(0) == -1.000:
		if keys[K_LEFT]:
			ship.heading -= ship.turnmultipler
		#if keys[K_RIGHT] or joystick.get_hat(0) == (1,0) or joystick.get_axis(0) > 0:
		if keys[K_RIGHT]:
			ship.heading += ship.turnmultipler
		ship.velocity['x'] = ship.velocity['x']*ship.dampening 
		ship.velocity['y'] = ship.velocity['y']*ship.dampening + 1.6

		screen.fill(black)

		ship.update()
		ship.draw()
		if flame:
			ship.draw_flame()
		landingpad.draw()
		for terrain in terrains:
			terrain.draw()
		text = 'Score: ' + str(score)
		textsurface = font.render(text, False, white)
		screen.blit(textsurface,(0,0))
		text = 'Fuel: ' + str(ship.fuel)
		if ship.fuel != 0:
			textsurface = font.render(text, False, green)
		else:
			textsurface = font.render(text, False, red)
		screen.blit(textsurface,(0,20))
		text = 'Speed: ' + str(int(ship.velocity['y'] * -1))
		if ship.velocity['y'] <= 40:
			textsurface = font.render(text, False, green)
		else:
			textsurface = font.render(text, False, red)
		screen.blit(textsurface,(0,40))
		text = 'Angle: ' + str(int(ship.heading))
		if int(ship.heading) in xrange(-15,15):
			textsurface = font.render(text, False, green)
		else:
			textsurface = font.render(text, False, red)
		screen.blit(textsurface,(0,60))
		if ship.pos['y'] + 15 >= screen.get_height():
			done = True
		for terrain in terrains:
			if int(ship.pos['y']) + 15 >= terrain.height:
				if terrain.landed(ship):
					done = True

		if int(ship.pos['y']) + 15 == landingpad.height:
			if landingpad.landed(ship):
				if int(ship.heading) in xrange(-15,15):
					if ship.velocity['y'] <= 40:
						winstate = 'Win'
						score += 100
						score -= int(ship.velocity['y'])
						if ship.heading < 0:
							score -= int(ship.heading * -1)
						else:
							score -= int(ship.heading)
						done = True
					else:
						done = True
				else:
					done = True
		elif int(ship.pos['y']) + 15 > landingpad.height:
			if landingpad.landed(ship):
				done = True

		pygame.display.flip()
		clock.tick(fps)

	textsurface = font.render(winstate, False, white)
	screen.blit(textsurface,(screen.get_width()/2,screen.get_height()/2))
	pygame.display.flip()
	return winstate

while True:
	winstate = main()
	if winstate == 'Win':
		global score
		#score += 100
	else:
		score = 0
	sleep(3)
