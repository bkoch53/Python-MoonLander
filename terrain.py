import pygame
from random import randint

class terrain():
	def __init__(self, screen):
		self.screen = screen
		self.color = (255,255,255)
		rx = randint(0,screen.get_width())
		ry = randint(screen.get_height()/2,screen.get_height())
		self.height = ry
		self.pl = [(rx,screen.get_height()),(rx,ry),(rx+30,ry),(rx+30,screen.get_height())]

	def landed(self, ship):
		if int(ship.pos['x']) in list(xrange(self.pl[1][0], self.pl[2][0])):
			return True
		else:
			return False

	def draw(self):
		pygame.draw.aalines(self.screen, self.color, False, self.pl, True)
