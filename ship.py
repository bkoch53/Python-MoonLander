import pygame
from math import radians, sin, cos
from random import randint
from time import sleep

class ship():
	def __init__(self, screen, turnspeed):
		self.screen = screen
		self.turnspeed = turnspeed
		self.color = (255,255,255)
		self.scale = 0.5
		self.pos = {'x': self.screen.get_width()/2, 'y': self.screen.get_height()/2-100}
		self.rel_pointlist = [(20,0),(0,55),(31,35),(60,55)]
                self.rel_pointlist = [(271,218),(217,218),(199,253),(127,289),(181,343),(307,343),(360,289),(127,289),(360,289),(289,253),(199,253),(289,253)]
		self.rel_flame = [(8,47),(31,60),(52,47)]
		for point in self.rel_pointlist:
			pin = self.rel_pointlist.index(point)
			self.rel_pointlist[pin] = (radians(point[0]),self.scale*point[1])
		for point in self.rel_flame:
			pi = self.rel_flame.index(point)
			self.rel_flame[pi] = (radians(point[0]),self.scale*point[1])
		self.heading = 0
		self.turnmultipler = 2.3
		self.velocity = {'x': 0.0, 'y': 0.0}
		self.thrust = 350.0
		self.dampening = 0.99
		self.fuel = 180

	def update(self):
		self.pos['x'] += self.velocity['x']*self.turnspeed
		self.pos['y'] += self.velocity['y']*self.turnspeed
		if int(self.heading) >= 360:
			self.heading = 0
		if int(self.heading) <= -360:
			self.heading = 0
		# Horizontal wrap
		if self.pos['x'] < 0:
			self.pos['x'] = self.screen.get_width()
		elif self.pos['x'] > self.screen.get_width():
			self.pos['x'] = 0
		# Vertical wrap
		if self.pos['y'] < 0:
			self.pos['y'] = 0
		elif self.pos['y'] + 15 > self.screen.get_height():
			self.pos['y'] = self.screen.get_height() - 15

		self.real_pointlist = []
		for point_angle,point_radius in self.rel_pointlist:
			angle = radians(self.heading - 64) + point_angle
			xp = point_radius * -sin(angle)
			yp = point_radius * cos(angle)
			self.real_pointlist.append((
				self.pos['x'] + xp,
				self.pos['y'] + yp
		))

		#self.real_pointlist = []
		#for point_angle, point_radius in self.rel_pointlist:
		#	angle = radians(self.heading - 63) + point_angle
		#	point_radius += 280 * self.scale
		#	xp = point_radius * -sin(angle)
		#	yp = point_radius * cos(angle)
		#	self.real_pointlist.append(
		#		(self.pos['x']+ xp, self.pos['y']+ yp)
		#	)
		self.real_flame = []
		for point_angle, point_radius in self.rel_flame:
			angle = radians(self.heading - 27) + point_angle
			point_radius -= 22 * self.scale
			xp = point_radius * -sin(angle)
			yp = point_radius * cos(angle)
			self.real_flame.append(
				(self.pos['x']+ xp, self.pos['y']+ yp)
			)

	def collision(self, asteroid):
		for point in self.real_flame:
			if asteroid.occluder.intersects(point):
				return True
			else:
				return False
	def blowup(self):
		pass

	def draw_flame(self):
		if randint(0,500) % 5 == 0:
			pygame.draw.aalines(self.screen, self.color, False, self.real_flame, True)

	def draw(self):
		# params: surface, color, closed, pointlist, blend
		pygame.draw.aalines(self.screen, self.color, True, self.real_pointlist, True)
